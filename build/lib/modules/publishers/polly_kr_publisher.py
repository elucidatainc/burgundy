from modules.publisher import publisher

class polly_kr_publisher(publisher):
    _registry_keys = ['kr']

    def __init__(self):
        pass

    def publish(self,path,data):
        import requests
        url = data['url']
        url = url + '?repo=' + data['pid'] + '/' + data['repo']
        args = { 'project' : data['pid'], 'repo' : data['repo'],  'path':'report_%s'%data['wfid'] }
        files = {'file' : (path , open(path,'rb').read(),'multipart/form-data') }
        resp = requests.post(url, files = files,data = args)
