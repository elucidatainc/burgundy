from modules.publisher import publisher

class polly_kr_publisher(publisher):
    _registry_keys = ['kr']

    def __init__(self):
        pass

    def publish(self,path,data):
        import requests
        url = data['url']
        url = url + '?repo=' + data['pid'] + '/' + data['repo']
        args = { 'project' : data['pid'], 'repo' : data['repo'],  'path':'report_%s'%data['wfid'] }
        files = {'file' : (path , open(path,'rb').read(),'multipart/form-data') }
        resp = requests.post(url, files = files,data = args)
        print("---RESPONSE SENT BY KR---")
        print(resp.text)
        print("----END OF RESPONSE----")
        post_host = url.split('/')[2]
        hash_key = url.split('/')[3]
        post_url = "https://{host}/{hashkey}/post/{pid}/{repo}/report_{wfid}.kp?repo={pid}/{repo}".format(host = post_host, hashkey=hash_key, pid = data['pid'],repo=data['repo'],wfid=data['wfid'])
        return post_url
