from modules.publisher import publisher
from botocore.exceptions import ClientError
import boto3
class jupyter_publisher(publisher):
    _registry_keys = ['ipynb']

    def __init__(self):
        pass

    def publish(self,path,data):
        import os 
        os.system("nbpublish  -lb -f html_ipypublish_nocode {path} -o /tmp/".format(path = path))
        os.system("nbpublish -pdf -lb -f latex_ipypublish_nocode {path} -o /tmp/".format(path = path))
        file_name_html = path[:-5] + 'html'
        file_name_pdf = path[:-5] +  'pdf' #This is included in the PDF publishing code
        # Upload the file
        bucket = os.environ["BUCKET_NAME"]
        project_id = os.environ["PROJECT_ID"]
        ocr_id = os.environ["OCR_ID"]
        s3_client = boto3.client('s3')
        html_object_name = '{project_id}/{ocr_id}/{ocr_id}.html'.format(project_id = project_id, ocr_id = ocr_id)
        pdf_object_name = '{project_id}/{ocr_id}/{ocr_id}.pdf'.format(project_id = project_id, ocr_id = ocr_id) 
        try:
            response_html = s3_client.upload_file(file_name_html, bucket, html_object_name, 
                                                    ExtraArgs={'ContentType': 'text/html'})
            response_pdf = s3_client.upload_file(file_name_pdf, bucket, pdf_object_name,
                                                    ExtraArgs={'ContentType': 'application/pdf'})
            status = True
        except ClientError as e:
            status =  False
        post_url = '/projects/{project_id}/ocr/{ocr_id}/{ocr_id}'.format(project_id = project_id, ocr_id = ocr_id)
        return status, post_url
