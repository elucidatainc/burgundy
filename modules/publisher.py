from modules.utils.registry import SubclassRegisteringABCMeta
from future.utils import with_metaclass

class publisher(with_metaclass(SubclassRegisteringABCMeta,object)):
    _registry_keys = None

    def publish(self, *args, **kwargs):
        pass
    @classmethod
    def get_publisher(cls,key):
        return cls._get_subclass_for(key)
