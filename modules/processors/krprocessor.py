from modules.processor import processor

class krprocessor(processor):
    _registry_keys = ['kr']

    def __init__(self):
        pass
    def create_tf(self):
        import uuid
        self.tf = open('/tmp/tf_%s.ipynb'%str(uuid.uuid4()),'w')
        return self.tf

    def dispose_tf(self):
        import os
        os.remove(self.tf.name)

    def process(self,template,data):
        import json
        self.create_tf()
        for key in data:
            data[key] = json.dumps(data[key])[1:-1]
        output = template.format(**data)
        self.tf.write(output)
        self.tf.close()
        return self.tf
        
