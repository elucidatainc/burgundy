from future.utils  import with_metaclass
from modules.utils.registry import SubclassRegisteringABCMeta

class processor(with_metaclass(SubclassRegisteringABCMeta, object)):
    _registry_keys = None
    @classmethod
    def get_processor(cls,key):
        return cls._get_subclass_for(key)

    def process(self, template, data):
        pass

