# Input:
# Template : type of processor ; type of publishing ; template
# filepath of where all the jsons are kept
# Call the processor with template string and data path : This returns file on local
# Call publisher with local file
# Delete temp files
# Return status 
from modules.publisher import publisher
from modules.processor import processor
import os
class Burgundy():
    def __init__(self):
        pass

    def __collect_jsons(self,path):
        import os,json
        data = {}
        for filename in os.listdir(path):
            if filename.endswith('.json'):
                filepath = os.path.join(path,filename)
                current = json.loads(open(filepath).read())
                for key,val in current.items():
                    if key not in data.keys():
                        data[key] = val
                    else:
                        if isinstance(val,dict):
                            for k,v in val.items():
                                data[key][k] = v
        return data
                        

    def generate_report(self,templatepath,path):
        jsons = self.__collect_jsons(path)
        template = open(templatepath).read()
        
        if 'OCR_ID' in os.environ:
            proc_type = 'kr'
            publisher_type = 'ipynb'
        else:
            proc_type = jsons['params']['proc_type']
            publisher_type = jsons['params']['publisher_type']

        data = jsons['data']
        proc = processor.get_processor(proc_type)()
        outfile = proc.process(template,data)
        status, rval = publisher.get_publisher(publisher_type)().publish(outfile.name,data)
        proc.dispose_tf()
        return status, rval
if __name__ == "__main__":
    import sys
    report_gen = Burgundy()
    report_gen.generate_report(sys.argv[1],sys.argv[2])
    
