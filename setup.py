import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="burgundy", # Replace with your own username
    version="0.0.1",
    author="Abhirath Batra",
    author_email="abhirath.batra@elucidata.io",
    description="Report Generator and Publisher",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/elucidatainc/burgundy/src/master/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
